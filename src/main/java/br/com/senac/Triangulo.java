/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author Fernando
 */
public class Triangulo extends Area{
   
    private final double base ;
    private final double altura ;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double getArea() {
        return (this.altura * this.base)/2 ;
    }

}
