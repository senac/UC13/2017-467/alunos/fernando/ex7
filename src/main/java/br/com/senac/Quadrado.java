/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author Fernando
 */
public class Quadrado extends Area {

    private final double lado;
    

    public Quadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double getArea() {
       return this.lado * this.lado;
    }

}
