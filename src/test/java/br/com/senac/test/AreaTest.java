/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Circulo;
import br.com.senac.Quadrado;
import br.com.senac.Retangulo;
import br.com.senac.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fernando
 */
public class AreaTest {
    
    public AreaTest() {
    }
    
   @Test
    public void deveCalcularAreaDe314DoCirculo (){
        
        Circulo circulo = new Circulo(10);
        
        assertEquals(314, circulo.getArea(),0.01);
        
    }
    
    @Test
    public void deveCalcularAreaDe225DoQuadrado (){
        
        Quadrado quadrado = new Quadrado(15);
        
        assertEquals(225, quadrado.getArea(),0.01);
        
    }
    
    @Test
    public void deveCalcularAreaDe500DoRetangulo (){
        
        Retangulo retangulo = new Retangulo(20, 25);
        
        assertEquals(500, retangulo.getArea(),0.01);
        
    }
    
     @Test
    public void deveCalcularAreaDe100DoTriangulo (){
        
         Triangulo triangulo = new Triangulo(10, 20);
        
        assertEquals(100, triangulo.getArea(),0.01);
        
    }

    
}
